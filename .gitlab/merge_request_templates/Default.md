## What does this MR do and why?

<!--
Describe in detail what your merge request does and why.

Please keep this description updated with any discussion that takes place so
that reviewers can understand your intent. Keeping the description updated is
especially important if they didn't participate in the discussion.
-->

%{first_multiline_commit}

## Screenshots or screen recordings

_Screenshots are required for UI changes, and strongly recommended for all other merge requests._

<!--
Please include any relevant screenshots or screen recordings that will assist
reviewers and future readers. If you need help visually verifying the change,
please leave a comment and ping a GitLab reviewer, maintainer, or MR coach.
-->

| Before | After  |
| ------ | ------ |
|        |        |

## How to set up and validate locally

_Numbered steps to set up and validate the change are strongly suggested._

## MR acceptance checklist

This checklist encourages us to confirm any changes have been analyzed to reduce risks in quality, performance, reliability, security, and maintainability.

* [ ] I have evaluated the [MR acceptance checklist](https://docs.gitlab.com/ee/development/code_review.html#acceptance-checklist) for this MR.

<!-- template sourced from https://gitlab.com/gitlab-org/remote-development/devfile-gem/-/blob/main/.gitlab/merge_request_templates/Default.md -->

/assign me
/label ~"Category:Workspaces" ~"group::remote development"