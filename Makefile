.PHONY: clean
clean:
	rm -f devfile-*.gem

.PHONY: go-build
go-build: clean
	mkdir -p bin && \
	rm -f bin/devfile && \
	cd ext && \
		go build -o ../bin/devfile

# This must be manually run on each platform
.PHONY: gem-build
gem-build: go-build
	gem build devfile.gemspec

.PHONY: test
test: go-build
	rspec

.PHONY: build-images
build-images: 
	cd build && \
	docker build --platform=linux/amd64 -t registry.gitlab.com/gitlab-org/ruby/gems/devfile-gem:ruby30 -f Dockerfile-ruby30 . && \
	docker build --platform=linux/amd64 -t registry.gitlab.com/gitlab-org/ruby/gems/devfile-gem:ruby31 -f Dockerfile-ruby31 . && \
	docker build --platform=linux/amd64 -t registry.gitlab.com/gitlab-org/ruby/gems/devfile-gem:ruby32 -f Dockerfile-ruby32 .

.PHONY: push-build-images
push-build-images: build-images
	docker push registry.gitlab.com/gitlab-org/ruby/gems/devfile-gem:ruby30 && \
	docker push registry.gitlab.com/gitlab-org/ruby/gems/devfile-gem:ruby31 && \
	docker push registry.gitlab.com/gitlab-org/ruby/gems/devfile-gem:ruby32
