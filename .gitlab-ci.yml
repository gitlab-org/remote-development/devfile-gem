---

###################################
#
# GENERAL/DEFAULT CONFIG:
#
###################################

include:
  - template: 'Workflows/MergeRequest-Pipelines.gitlab-ci.yml'
  - component: gitlab.com/components/secret-detection/secret-detection@~latest
  - template: Security/Dependency-Scanning.gitlab-ci.yml
  - local: '.gitlab/ci/gem-release.yml'

default:
  tags:
    - gitlab-org
  interruptible: true # All jobs are interruptible by default
  # The following 'retry' configuration settings may help avoid false build failures
  #  during brief problems with CI/CD infrastructure availability
  retry:
    max: 2 # This is confusing, but this means "3 runs at max".
    when:
      - unknown_failure
      - api_failure
      - runner_system_failure
      - job_execution_timeout
      - stuck_or_timeout_failure

variables:
  BUNDLE_FROZEN: 'true'

  # NOTE: The following PERFORMANCE and RELIABILITY variables are optional, but may
  #       improve performance of larger repositories, or improve reliability during
  #       brief problems with CI/CD infrastructure availability

  ### PERFORMANCE ###
  # GIT_* variables to speed up repo cloning/fetching
  GIT_DEPTH: 10
  # Disabling LFS and submodules will speed up jobs, because runners don't have to perform
  # the submodule steps during repo clone/fetch. These settings can be deleted if you are using
  # LFS or submodules.
  GIT_LFS_SKIP_SMUDGE: 1
  GIT_SUBMODULE_STRATEGY: none

  ### RELIABILITY ###
  # Reduce potential of flaky builds via https://docs.gitlab.com/ee/ci/yaml/#job-stages-attempts variables
  GET_SOURCES_ATTEMPTS: 3
  ARTIFACT_DOWNLOAD_ATTEMPTS: 3
  RESTORE_CACHE_ATTEMPTS: 3
  EXECUTOR_JOB_SECTION_ATTEMPTS: 3

####################################
##
## TEST STAGE
##
####################################

.rspec:
  before_script:
    - ruby -v                                   # Print out ruby version for debugging
    - gem install bundler --no-document         # Bundler is not installed with the image
    - bundle install
  script:
    - make test

rspec:
  extends: .rspec
  image: "registry.gitlab.com/gitlab-org/ruby/gems/devfile-gem:ruby$RUBY_VERSION"
  parallel:
    matrix:
      - RUBY_VERSION:
        - "30"
        - "31"
        - "32"

rubocop:
  image: "registry.gitlab.com/gitlab-org/ruby/gems/devfile-gem:ruby30"
  script:
    - gem install bundler --no-document         # Bundler is not installed with the image
    - bundle install
    - bundle exec rubocop
