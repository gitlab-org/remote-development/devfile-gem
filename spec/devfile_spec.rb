# frozen_string_literal: true

require 'devfile'
require 'yaml'

shared_examples 'common devfile tests' do
  it '#get_deployment returns a valid kubernetes deployment yaml' do
    deployment = Devfile::Parser.get_deployment(valid_devfile, name, namespace, labels_str, annotations_str, replicas)
    actual_resources = YAML.safe_load(deployment)

    expect(actual_resources).to eq expected_deployment
  end

  it '#get_service returns a valid kubernetes service yaml' do
    service = Devfile::Parser.get_service(valid_devfile, name, namespace, labels_str, annotations_str)
    actual_resources = YAML.safe_load(service)

    expect(actual_resources).to eq expected_service
  end

  it '#get_ingress returns a valid kubernetes ingress yaml' do
    ingress = Devfile::Parser.get_ingress(
      valid_devfile,
      name,
      namespace,
      labels_str,
      annotations_str,
      domain_template,
      ingress_class
    )
    actual_resources = YAML.safe_load(ingress)

    expect(actual_resources).to eq expected_ingress
  end

  it '#get_ingress throws an error if invalid domain template is passed' do
    domain_template = invalid_domain_template

    expect do
      Devfile::Parser.get_ingress(
        valid_devfile,
        name,
        namespace,
        labels_str,
        annotations_str,
        domain_template,
        ingress_class
      )
    end.to raise_error(Devfile::CliError, /template: domainTemplate:1: function "port" not defined/)
  end

  it '#get_ingress returns empty string when ingressClass is none' do
    ingress_class = none_ingress_class

    ingress = Devfile::Parser.get_ingress(
      valid_devfile,
      name,
      namespace,
      labels_str,
      annotations_str,
      domain_template,
      ingress_class
    )

    expect(ingress).to eq ''
  end

  it '#get_all returns a valid kubernetes resources yaml' do
    resources = Devfile::Parser.get_all(
      valid_devfile,
      name,
      namespace,
      labels_str,
      annotations_str,
      replicas,
      domain_template,
      ingress_class
    )
    actual_resources = YAML.load_stream(resources)

    expect(actual_resources).to eq expected_all_resources
  end

  it '#get_all throws an error if invalid domain template is passed' do
    domain_template = invalid_domain_template

    expect do
      Devfile::Parser.get_all(
        valid_devfile,
        name,
        namespace,
        labels_str,
        annotations_str,
        replicas,
        domain_template,
        ingress_class
      )
    end.to raise_error(Devfile::CliError, /template: domainTemplate:1: function "port" not defined/)
  end

  it '#get_all returns all resources except for ingress when runtimeClass is none' do
    ingress_class = none_ingress_class

    resources = Devfile::Parser.get_all(
      valid_devfile,
      name,
      namespace,
      labels_str,
      annotations_str,
      replicas,
      domain_template,
      ingress_class
    )
    actual_resources = YAML.load_stream(resources)

    expect(actual_resources).to eq expected_all_resources_without_ingress
  end

  it '#flatten returns a flattened devfile json' do
    flattened_devfile = Devfile::Parser.flatten(valid_devfile)
    actual_flattened_devfile = YAML.safe_load(flattened_devfile)

    expect(actual_flattened_devfile).to eq expected_flattened_devfile
  end
end

RSpec.describe Devfile do
  let(:labels_str) do
    File.read(File.expand_path('fixtures/labels.yaml', File.dirname(__FILE__)))
  end

  let(:annotations_str) do
    File.read(File.expand_path('fixtures/annotations.yaml', File.dirname(__FILE__)))
  end

  let(:valid_domain_template) do
    File.read(File.expand_path('fixtures/valid_domain_template.tmpl', File.dirname(__FILE__)))
  end

  let(:invalid_domain_template) do
    File.read(File.expand_path('fixtures/invalid_domain_template.tmpl', File.dirname(__FILE__)))
  end

  let(:domain_template) { valid_domain_template }

  let(:valid_ingress_class) do
    File.read(File.expand_path('fixtures/valid_ingress_class.txt', File.dirname(__FILE__)))
  end

  let(:none_ingress_class) do
    File.read(File.expand_path('fixtures/none_ingress_class.txt', File.dirname(__FILE__)))
  end

  let(:ingress_class) { valid_ingress_class }
  let(:name) { 'test' }
  let(:namespace) { 'default' }
  let(:replicas) { 1 }
  let(:devfile_version) { '2.2.0' }

  let(:expected_deployment) do
    YAML.safe_load(%(
      apiVersion: apps/v1
      kind: Deployment
      metadata:
        annotations:
          example: value
        creationTimestamp: null
        labels:
          name: test
        name: test
        namespace: default
      spec:
        replicas: 1
        selector:
          matchLabels:
            name: test
        strategy:
          type: Recreate
        template:
          metadata:
            annotations:
              example: value
            creationTimestamp: null
            labels:
              name: test
            name: test
            namespace: default
          spec:
            containers:
            - args:
              - tail
              - -f
              - /dev/null
              env:
              - name: PROJECTS_ROOT
                value: /projects
              - name: PROJECT_SOURCE
                value: /projects
              image: registry.access.redhat.com/ubi9/go-toolset:1.18.10-4
              imagePullPolicy: Always
              name: runtime
              ports:
              - containerPort: 8080
                name: http-go
                protocol: TCP
              resources:
                limits:
                  memory: 1Gi
            - env:
              - name: PROJECTS_ROOT
                value: /projects
              - name: PROJECT_SOURCE
                value: /projects
              image: quay.io/devfile/golang:latest
              imagePullPolicy: Always
              name: runtime-2
              ports:
              - containerPort: 8081
                name: http
                protocol: TCP
              resources:
                limits:
                  memory: 1Gi
              volumeMounts:
              - mountPath: /ephemeral-volume
                name: ephemeral-volume
              - mountPath: /persistent-volume
                name: persistent-volume
            initContainers:
            - env:
              - name: PROJECTS_ROOT
                value: /projects
              - name: PROJECT_SOURCE
                value: /projects
              image: quay.io/devfile/golang:sidecar1
              imagePullPolicy: Always
              name: sidecar1-sidecar1-command-1
              resources: {}
              volumeMounts:
              - mountPath: /ephemeral-volume
                name: ephemeral-volume
              - mountPath: /persistent-volume
                name: persistent-volume
            - env:
              - name: PROJECTS_ROOT
                value: /projects
              - name: PROJECT_SOURCE
                value: /projects
              image: quay.io/devfile/golang:sidecar2
              imagePullPolicy: Always
              name: sidecar2-sidecar2-command-2
              resources: {}
              volumeMounts:
              - mountPath: /persistent-volume
                name: persistent-volume
            volumes:
            - emptyDir: {}
              name: ephemeral-volume
            - name: persistent-volume
              persistentVolumeClaim:
                claimName: test-persistent-volume
            - name: persistent-volume-not-mounted
              persistentVolumeClaim:
                claimName: test-persistent-volume-not-mounted
      status: {}
    ))
  end

  let(:expected_service) do
    YAML.safe_load(%(
      apiVersion: v1
      kind: Service
      metadata:
        annotations:
          example: value
        creationTimestamp: null
        labels:
          name: test
        name: test
        namespace: default
      spec:
        ports:
        - name: http-go
          port: 8080
          targetPort: 8080
        - name: http
          port: 8081
          targetPort: 8081
        selector:
          name: test
      status:
        loadBalancer: {}
    ))
  end

  let(:expected_ingress) do
    YAML.safe_load(%(
      apiVersion: networking.k8s.io/v1
      kind: Ingress
      metadata:
        annotations:
          example: value
        creationTimestamp: null
        labels:
          name: test
        name: test
        namespace: default
      spec:
        ingressClassName: ingress
        rules:
        - host: 8080-workspace.example.com
          http:
            paths:
            - backend:
                service:
                  name: test
                  port:
                    number: 8080
              path: /
              pathType: Prefix
        - host: 8081-workspace.example.com
          http:
            paths:
            - backend:
                service:
                  name: test
                  port:
                    number: 8081
              path: /
              pathType: Prefix
      status:
        loadBalancer: {}
    ))
  end

  let(:expected_pvc) do
    YAML.load_stream(
      <<~RESOURCES_YAML
        apiVersion: v1
        kind: PersistentVolumeClaim
        metadata:
          annotations:
            example: value
          creationTimestamp: null
          labels:
            name: test
          name: test-persistent-volume
          namespace: default
        spec:
          accessModes:
          - ReadWriteOnce
          resources:
            requests:
              storage: 2Gi
        status: {}
        ---
        apiVersion: v1
        kind: PersistentVolumeClaim
        metadata:
          annotations:
            example: value
          creationTimestamp: null
          labels:
            name: test
          name: test-persistent-volume-not-mounted
          namespace: default
        spec:
          accessModes:
          - ReadWriteOnce
          resources:
            requests:
              storage: 3Gi
        status: {}
    RESOURCES_YAML
    )
  end

  let(:expected_all_resources) do
    YAML.load_stream(
      <<~RESOURCES_YAML
        #{YAML.dump(expected_deployment)}
        #{YAML.dump(expected_service)}
        #{YAML.dump(expected_ingress)}
        #{expected_pvc.map { |x| YAML.dump(x) }.join}
    RESOURCES_YAML
    )
  end

  let(:expected_all_resources_without_ingress) do
    YAML.load_stream(
      <<~RESOURCES_YAML
        #{YAML.dump(expected_deployment)}
        #{YAML.dump(expected_service)}
        #{expected_pvc.map { |x| YAML.dump(x) }.join}
    RESOURCES_YAML
    )
  end

  let(:expected_flattened_devfile) do
    YAML.safe_load(%(
      commands:
      - attributes:
          api.devfile.io/imported-from: 'id: go, registryURL: https://registry.devfile.io'
        exec:
          commandLine: go build main.go
          component: runtime
          env:
          - name: GOPATH
            value: ${PROJECT_SOURCE}/.go
          - name: GOCACHE
            value: ${PROJECT_SOURCE}/.cache
          group:
            isDefault: true
            kind: build
          hotReloadCapable: false
          workingDir: ${PROJECT_SOURCE}
        id: build
      - attributes:
          api.devfile.io/imported-from: 'id: go, registryURL: https://registry.devfile.io'
        exec:
          commandLine: ./main
          component: runtime
          group:
            isDefault: true
            kind: run
          hotReloadCapable: false
          workingDir: ${PROJECT_SOURCE}
        id: run
      - apply:
          component: sidecar1
        id: sidecar1-command
      - apply:
          component: sidecar2
        id: sidecar2-command
      components:
      - attributes:
          api.devfile.io/imported-from: 'id: go, registryURL: https://registry.devfile.io'
        container:
          args:
          - tail
          - -f
          - /dev/null
          dedicatedPod: false
          endpoints:
          - name: http-go
            secure: false
            targetPort: 8080
          image: registry.access.redhat.com/ubi9/go-toolset:1.18.10-4
          memoryLimit: 1024Mi
          mountSources: true
        name: runtime
      - container:
          dedicatedPod: false
          endpoints:
          - exposure: public
            name: http
            secure: false
            targetPort: 8081
          image: quay.io/devfile/golang:latest
          memoryLimit: 1024Mi
          mountSources: true
          volumeMounts:
          - name: ephemeral-volume
            path: /ephemeral-volume
          - name: persistent-volume
            path: /persistent-volume
        name: runtime-2
      - container:
          dedicatedPod: false
          image: quay.io/devfile/golang:sidecar1
          mountSources: true
          volumeMounts:
          - name: ephemeral-volume
            path: /ephemeral-volume
          - name: persistent-volume
            path: /persistent-volume
        name: sidecar1
      - container:
          dedicatedPod: false
          image: quay.io/devfile/golang:sidecar2
          mountSources: true
          volumeMounts:
          - name: persistent-volume
            path: /persistent-volume
        name: sidecar2
      - name: ephemeral-volume
        volume:
          ephemeral: true
          size: 1Gi
      - name: persistent-volume
        volume:
          ephemeral: false
          size: 2Gi
      - name: persistent-volume-not-mounted
        volume:
          ephemeral: false
          size: 3Gi
      events:
        preStart:
        - sidecar1-command
        - sidecar2-command
      metadata:
        language: Go
        name: go
      schemaVersion: #{devfile_version}
      starterProjects:
      - attributes:
          api.devfile.io/imported-from: 'id: go, registryURL: https://registry.devfile.io'
        description: A Go project with a simple HTTP server
        git:
          checkoutFrom:
            revision: main
          remotes:
            origin: https://github.com/devfile-samples/devfile-stack-go.git
        name: go-starter
    ))
  end

  context 'when a valid devfile is provided' do
    context 'when devfile version is 2.2.0' do
      let(:valid_devfile) do
        File.read(File.expand_path('fixtures/.valid.2.2.0.devfile.yaml', File.dirname(__FILE__)))
      end

      include_examples 'common devfile tests'
    end

    context 'when devfile version is 2.2.1' do
      let(:devfile_version) { '2.2.1' }
      let(:valid_devfile) do
        File.read(File.expand_path('fixtures/.valid.2.2.1.devfile.yaml', File.dirname(__FILE__)))
      end

      include_examples 'common devfile tests'
    end

    context 'when devfile version is 2.2.2' do
      let(:devfile_version) { '2.2.2' }
      let(:valid_devfile) do
        File.read(File.expand_path('fixtures/.valid.2.2.2.devfile.yaml', File.dirname(__FILE__)))
      end

      include_examples 'common devfile tests'
    end

    context 'when devfile version is 2.3.0' do
      let(:devfile_version) { '2.3.0' }
      let(:valid_devfile) do
        File.read(File.expand_path('fixtures/.valid.2.3.0.devfile.yaml', File.dirname(__FILE__)))
      end

      include_examples 'common devfile tests'
    end
  end

  context 'when an invalid devfile having extra field is provided' do
    let(:invalid_devfile) do
      File.read(File.expand_path('fixtures/.invalid.extra-field-devfile.yaml', File.dirname(__FILE__)))
    end
    let(:error_str) { /Additional property random is not allowed/ }

    it '#get_deployment throws an error' do
      expect do
        Devfile::Parser.get_deployment(
          invalid_devfile,
          name,
          namespace,
          labels_str,
          annotations_str,
          replicas
        )
      end.to raise_error(Devfile::CliError, error_str)
    end

    it '#get_service throws an error' do
      expect do
        Devfile::Parser.get_service(invalid_devfile, 'test', 'default', labels_str, annotations_str)
      end.to raise_error(Devfile::CliError, error_str)
    end

    it '#get_ingress throws an error' do
      expect do
        Devfile::Parser.get_ingress(
          invalid_devfile,
          name,
          namespace,
          labels_str,
          annotations_str,
          domain_template,
          ingress_class
        )
      end.to raise_error(Devfile::CliError, error_str)
    end
    it '#get_ingress throws an error' do
      expect do
        Devfile::Parser.get_ingress(
          invalid_devfile,
          name,
          namespace,
          labels_str,
          annotations_str,
          domain_template,
          ingress_class
        )
      end.to raise_error(Devfile::CliError, error_str)
    end

    it '#get_all throws an error' do
      expect do
        Devfile::Parser.get_all(
          invalid_devfile,
          name,
          namespace,
          labels_str,
          annotations_str,
          replicas,
          domain_template,
          ingress_class
        )
      end.to raise_error(Devfile::CliError, error_str)
    end

    it '#flatten throws an error' do
      expect do
        Devfile::Parser.flatten(invalid_devfile)
      end.to raise_error(Devfile::CliError, error_str)
    end
  end

  context 'when an invalid devfile having multiple endpoints with same port is provided' do
    let(:invalid_devfile) do
      File.read(File.expand_path('fixtures/.invalid.multiple-endpoints-same-port-devfile.yaml', File.dirname(__FILE__)))
    end
    let(:error_str) { /devfile contains multiple containers with same endpoint targetPort: 8080/ }

    it '#get_deployment throws an error' do
      expect do
        Devfile::Parser.get_deployment(
          invalid_devfile,
          name,
          namespace,
          labels_str,
          annotations_str,
          replicas
        )
      end.to raise_error(Devfile::CliError, error_str)
    end

    it '#get_service throws an error' do
      expect do
        Devfile::Parser.get_service(invalid_devfile, 'test', 'default', labels_str, annotations_str)
      end.to raise_error(Devfile::CliError, error_str)
    end

    it '#get_ingress throws an error' do
      expect do
        Devfile::Parser.get_ingress(
          invalid_devfile,
          name,
          namespace,
          labels_str,
          annotations_str,
          domain_template,
          ingress_class
        )
      end.to raise_error(Devfile::CliError, error_str)
    end
    it '#get_ingress throws an error' do
      expect do
        Devfile::Parser.get_ingress(
          invalid_devfile,
          name,
          namespace,
          labels_str,
          annotations_str,
          domain_template,
          ingress_class
        )
      end.to raise_error(Devfile::CliError, error_str)
    end

    it '#get_all throws an error' do
      expect do
        Devfile::Parser.get_all(
          invalid_devfile,
          name,
          namespace,
          labels_str,
          annotations_str,
          replicas,
          domain_template,
          ingress_class
        )
      end.to raise_error(Devfile::CliError, error_str)
    end

    it '#flatten throws an error' do
      expect do
        Devfile::Parser.flatten(invalid_devfile)
      end.to raise_error(Devfile::CliError, error_str)
    end
  end
end
