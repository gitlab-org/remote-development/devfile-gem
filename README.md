# Ruby Devfile Gem

This gem generates Kubernetes manifest YAMLs from a [Devfile](https://devfile.io). This gem wraps the existing go [Devfile library](https://github.com/devfile/library) as a binary.

### Local Development

#### Run CLI directly

```shell
cd ext

go run . deployment \
  "$(cat ./../spec/fixtures/.valid.2.3.0.devfile.yaml)" \
  test \
  default \
  "$(cat ./../spec/fixtures/labels.yaml)" \
  "$(cat ./../spec/fixtures/annotations.yaml)" \
  1

go run . service \
  "$(cat ./../spec/fixtures/.valid.2.3.0.devfile.yaml)" \
  test \
  default \
  "$(cat ./../spec/fixtures/labels.yaml)" \
  "$(cat ./../spec/fixtures/annotations.yaml)"

go run . ingress \
  "$(cat ./../spec/fixtures/.valid.2.3.0.devfile.yaml)" \
  test \
  default \
  "$(cat ./../spec/fixtures/labels.yaml)" \
  "$(cat ./../spec/fixtures/annotations.yaml)" \
  "$(cat ./../spec/fixtures/valid_domain_template.tmpl)" \
  "$(cat ./../spec/fixtures/valid_ingress_class.txt)"

go run . pvc \
  "$(cat ./../spec/fixtures/.valid.2.3.0.devfile.yaml)" \
  test \
  default \
  "$(cat ./../spec/fixtures/labels.yaml)" \
  "$(cat ./../spec/fixtures/annotations.yaml)"

go run . all \
  "$(cat ./../spec/fixtures/.valid.2.3.0.devfile.yaml)" \
  test \
  default \
  "$(cat ./../spec/fixtures/labels.yaml)" \
  "$(cat ./../spec/fixtures/annotations.yaml)" \
  1 \
  "$(cat ./../spec/fixtures/valid_domain_template.tmpl)" \
  "$(cat ./../spec/fixtures/valid_ingress_class.txt)"

go run . flatten \
  "$(cat ./../spec/fixtures/.valid.2.3.0.devfile.yaml)"
```


#### Install dependencies

```shell
bundle install --binstubs
```

#### Build

Note: When building locally, the gem will be built for the local cpu architecture / OS. For example, running the commands below on a macOS machine with M1 CPU will build a gem for arm64-darwin platform. In order to build the gem for `ruby` platform, set an environment variable `DEVFILE_GEMSPEC_PLATFORM` with value `ruby` before building the gem.

```shell
make go-build && make gem-build
gem spec ${GEM_FILE}
gem install ${GEM_FILE}
```

#### Run test

```shell
make test
```

#### Publishing a new version of the Gem

1. Ensure that the gem version was pre-incremented in the `devfile.gemspec` file
   immediately after the last release. If not, go ahead and make a separate commit
   to increment it.
2. After the last MR is merged into `main`, make a new tag from the last commit, and push to remote. For example, `git tag -a 0.1.0 84b05efd4a02f802fc0069c108904fe09833f29d -m 'v0.1.0'` and `git push origin 0.1.0`. 
3. By default, devfile-gem will be built for different platforms as a part of the CI of any MR. However, the new versions of the gem can only be published from a new tag pushed. This job to publish a new version can/should only be run by a maintainer.
4. After a new version of the gem has been pushed, proactively increment the minor version in the `devfile.gemspec` file, so that all subsequent changes until the next release are associated with the _next_ version of the gem. For example, if the version is currently `0.1.0`, increment it to `0.2.0`. If there is a need to release a major or patch version instead of a minor version, it can be updated accordingly before release.

#### Miscellaneous

- If you create any new Go file in `ext`, make sure it is added in `s.files` of `Gem::Specification` in `devfile.gemspec`
- If you update go version, update the corresponding values in `build/`

#### CI base image update

When updating the CI image, please keep an eye out for potential issues that might come up.

A good example is [this issue](https://gitlab.com/gitlab-org/gitlab/-/issues/499476) related to the required GLIBC version. After a CI image update, the minimum GLIBC version was bumped to 2.32. The problem is that CentOS only provides GLIBC 2.17 with no updates available since CentOS is deprecated. This caused issues for a customer using workspaces on CentOS.

It’s worth double-checking compatibility to avoid similar problems.

### Usage

```ruby
require 'devfile'

devfile = %{
schemaVersion: 2.3.0
metadata:
  name: go
  language: go
components:
  - container:
      endpoints:
        - name: http
          targetPort: 8080
      image: quay.io/devfile/golang:latest
      memoryLimit: 1024Mi
      mountSources: true
    name: runtime
}

labels_str = %{
name: test
}

annotations_str = %{
example: value
}

flattened_devfile = Devfile::Parser.flatten(devfile)
puts "===== Flattened Devfile ====="
puts flattened_devfile

deployment = Devfile::Parser.get_deployment(devfile, 'test', 'default', labels_str, annotations_str, 1)
puts "===== Deployment ====="
puts deployment

service = Devfile::Parser.get_service(devfile, 'test', 'default', labels_str, annotations_str)
puts "===== Service ====="
puts service

ingress = Devfile::Parser.get_ingress(devfile, 'test', 'default', labels_str, annotations_str, '{{.port}}-test.example.com', 'nginx')
puts "===== Ingress ====="
puts ingress

resources = Devfile::Parser.get_all(devfile, 'test', 'default', labels_str, annotations_str, 1, '{{.port}}-test.example.com', 'nginx')
puts "===== Kubernetes Resources ====="
puts resources
```
