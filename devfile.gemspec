# frozen_string_literal: true

Gem::Specification.new do |s|
  s.name = 'devfile'
  s.version = '0.2.0'
  s.licenses = ['MIT']
  s.summary = 'Parse and generate kubernetes manifests from a Devfile'
  s.description = 'Library used to generate kubernetes manifests from a Devfile.'
  s.authors = ['GitLab']
  s.email = %w[cwoolley@gitlab.com vtak@gitlab.com spatnaik@gitlab.com]
  s.files = %w[bin/devfile lib/devfile.rb ext/go.mod ext/go.sum ext/main.go ext/devfile.go ext/volume.go]
  s.require_paths = %w[lib]
  s.required_ruby_version = '>= 2.7'
  s.homepage = 'https://gitlab.com'
  s.metadata = { 'source_code_uri' => 'https://gitlab.com/gitlab-org/ruby/gems/devfile-gem' }

  # If DEVFILE_GEMSPEC_PLATFORM is missing at the time of building the gem
  # the platform will be derived from the OS/Arch of the host on which the gem is built
  # If DEVFILE_GEMSPEC_PLATFORM is explicitly stated(as in CI),
  # gemspec.platform will use the provided value
  s.platform = (ENV['DEVFILE_GEMSPEC_PLATFORM'] ||
    "#{Gem::Platform.local.cpu}-#{Gem::Platform.local.os}")
end
